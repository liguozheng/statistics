#pragma once

#include <QObject>
#include "windowsx.h"
#include <QSettings>
#include <QTextCodec>
#include <QSqlDatabase>
#include <QSqlQueryModel>
#include <QDir>
#include <QDateTime>
#include <QTextStream>
#pragma execution_character_set("utf-8")


extern     int g_CurrRepeatCount;
extern     int g_CurrOtherCartCount;
extern     int g_CurrProductCount;
extern     int g_CurrGuoBanCount;
extern     int g_CurrRejectCount;
extern     int g_CurrBuPiaoCount;
extern     int g_CurrwhiteCount;
extern     int g_CurrModifyCount;
extern     int g_CurrExchangeount;
extern     int g_CurrNotcheckCount;
extern     int g_CurrNoPrintCount;

extern     int g_OnlineCountState;
extern     QString g_OnlineReason;
extern     int g_nSampleCountStates;
extern     int g_OnlineCenterCountState;
extern     int g_nSampleType ;

extern QString g_CurrSelectCarNum;
extern QString g_ModidyOfCurrCart;
extern QString g_seldate;
extern QString g_selCardate;
extern int g_curSelCartID;

extern QString g_path;
class QGlobal : public QObject
{
	Q_OBJECT

public:
	QGlobal(QObject *parent);
	~QGlobal();
	void InfoPath();

	
private:
};
