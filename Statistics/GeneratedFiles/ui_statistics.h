/********************************************************************************
** Form generated from reading UI file 'statistics.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_STATISTICS_H
#define UI_STATISTICS_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDateEdit>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_StatisticsClass
{
public:
    QWidget *centralWidget;
    QHBoxLayout *horizontalLayout;
    QDateEdit *dateEdit_start;
    QLabel *label;
    QDateEdit *dateEdit_end;
    QSpacerItem *horizontalSpacer;
    QPushButton *pushButton;
    QSpacerItem *horizontalSpacer_2;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *StatisticsClass)
    {
        if (StatisticsClass->objectName().isEmpty())
            StatisticsClass->setObjectName(QStringLiteral("StatisticsClass"));
        StatisticsClass->resize(356, 139);
        centralWidget = new QWidget(StatisticsClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        horizontalLayout = new QHBoxLayout(centralWidget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        dateEdit_start = new QDateEdit(centralWidget);
        dateEdit_start->setObjectName(QStringLiteral("dateEdit_start"));

        horizontalLayout->addWidget(dateEdit_start);

        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));

        horizontalLayout->addWidget(label);

        dateEdit_end = new QDateEdit(centralWidget);
        dateEdit_end->setObjectName(QStringLiteral("dateEdit_end"));

        horizontalLayout->addWidget(dateEdit_end);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));

        horizontalLayout->addWidget(pushButton);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);

        StatisticsClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(StatisticsClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 356, 23));
        StatisticsClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(StatisticsClass);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        StatisticsClass->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(StatisticsClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        StatisticsClass->setStatusBar(statusBar);

        retranslateUi(StatisticsClass);

        QMetaObject::connectSlotsByName(StatisticsClass);
    } // setupUi

    void retranslateUi(QMainWindow *StatisticsClass)
    {
        StatisticsClass->setWindowTitle(QApplication::translate("StatisticsClass", "Statistics", 0));
        label->setText(QApplication::translate("StatisticsClass", "\342\200\224", 0));
        pushButton->setText(QApplication::translate("StatisticsClass", "\347\273\237\350\256\241", 0));
    } // retranslateUi

};

namespace Ui {
    class StatisticsClass: public Ui_StatisticsClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_STATISTICS_H
