#pragma once
#include<ActiveQt/QAxObject>
#include <QObject>
#include <QDir>
#include <QFileDialog>
#include <QtPrintSupport/QPrintDialog>
#include <QtPrintSupport/QPrinter>
#include "QGlobal.h"
class QExcelPrint : public QObject
{
	Q_OBJECT

public:
	QExcelPrint(QObject *parent);
	~QExcelPrint();
	bool exportToExcel();
	void setCellValue(QString row, int column, const QString &value);
	bool QExcelPrint::printview(QString ValPrintFile);
	QString m_filepath;
	QAxObject *m_excel;
	QAxObject *m_worksheet;
	QAxObject *m_workbook;//������
};
